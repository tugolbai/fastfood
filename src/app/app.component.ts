import {Component, Input} from '@angular/core';
import {Item} from "./shared/item.model";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @Input() total = 0;
  @Input() array: Item[] = [];

  items: Item[] = [
    new Item('https://www.thespruceeats.com/thmb/vJUFf6L4p8y9Cn_1pE9Z7Ua9uok=/3000x2001/filters:fill(auto,1)/' +
      'indian-style-burger-1957599-hero-01-266103a4bb4e4ee7b5feb4da2d2e99da.jpg', 'Hamburger', 1, 80),
    new Item('https://st2.depositphotos.com/3957801/5642/i/950/depositphotos_56423065-stock-photo-bacon-' +
      'burger.jpg', 'Cheeseburger', 1, 90),
    new Item('https://www.potatoprocess.com/wp-content/uploads/2017/01/Introduction-of-French-Fries-' +
      'Nutrition.jpg', 'Fries', 1, 45),
    new Item('https://thefoodqueen.com/wp-content/uploads/2020/12/How-to-make-strong-coffee.jpg',
      'Coffee', 1, 70),
    new Item('https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fstatic.onecms.io%2Fwp-content%2' +
      'Fuploads%2Fsites%2F9%2F2021%2F06%2F22%2Fdifferent-types-of-tea-FT-BLOG0621.jpg&q=85',
      'Tea', 1, 50),
    new Item('https://media.istockphoto.com/photos/cocacola-classic-in-a-glass-bottle-picture-id499208007?k=' +
      '20&m=499208007&s=612x612&w=0&h=ZGZi6HI8himQzfsWxH9Ak3CU2zFSGK4gyzAHnRLw7kY=',
      'Coca-Cola', 1, 40),
  ];

  getTotal() {
    let sum = 0;
    this.array.forEach(arr => {
      this.total = 0;
      sum = sum + arr.getPrice();
      this.total = this.total + sum;
    });
  }

  onClick(index: number) {
    const indexArr = this.array.indexOf(this.items[index]);
    if (!this.array.includes(this.items[index])) {
      this.array.push(this.items[index]);
      this.getTotal();
    } else {
      this.array[indexArr].amount++;
      this.getTotal();
    }
  }

  deleteButton(index: number) {
    if (this.array[index].amount > 0) {
      this.array.splice(index, 1);
      this.getTotal();
      if (this.array.length === 0) {
        this.total = 0;
      }
    } else {
      return;
    }
  }
}
