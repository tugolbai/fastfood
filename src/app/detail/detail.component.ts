import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Item} from "../shared/item.model";

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent  {
  @Input() item!: Item;
  @Output() delete = new EventEmitter();

  onDelete() {
    this.delete.emit();
  }
}
