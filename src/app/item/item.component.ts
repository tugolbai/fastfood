import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Item} from "../shared/item.model";

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent  {
  @Input() item!: Item;
  @Output() clickAdd = new EventEmitter();

  onClick() {
    this.clickAdd.emit();
  }
}
